package at.robin.car;

public class car {
	private float topspeed;
	private String color;
	private double fuelconsumtion;
	private String type;
	private float stickerprice;
	private float mileage;
	private manufacturer manufacturer;

	public float getTopspeed() {
		return topspeed;
	}

	public car(float topspeed, String color, double fuelconsumtion, String type, float stickerprice, float mileage,
			manufacturer manufacturer) {
		super();
		this.topspeed = topspeed;
		this.color = color;
		this.fuelconsumtion = fuelconsumtion;
		this.type = type;
		this.stickerprice = stickerprice;
		this.mileage = mileage;
		this.manufacturer = manufacturer;

	}

	public void setTopspeed(float topspeed) {
		this.topspeed = topspeed;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public double getFuelconsumtion() {
		return fuelconsumtion;
	}

	public void setFuelconsumtion(float fuelconsumtion) {
		this.fuelconsumtion = fuelconsumtion;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public float getStickerprice() {
		return stickerprice;
	}

	public void setStickerprice(float stickerprice) {
		this.stickerprice = stickerprice;
	}

	public float getMileage() {
		return mileage;
	}

	public double carprice() {
		return (manufacturer.getDiscount() / 100) * this.stickerprice;

	}

	public void setMileage(float mileage) {
		this.mileage = mileage;
	}

	public double getmileage() {
		if (this.mileage <= 50000) {
			this.fuelconsumtion = 7;
		} else {
			this.fuelconsumtion = (7 * 0.098);
		}
		System.out.println(this.fuelconsumtion);
		return this.fuelconsumtion;

	}

}
