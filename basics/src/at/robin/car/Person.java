package at.robin.car;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Person {

	private String Firstname;
	private String Lastname;
	@SuppressWarnings("deprecation")
	LocalDate Birthday;
	LocalDate today = LocalDate.now();

	private double price;

	private List<car> cars;

	public Person(String firstname, String lastname, LocalDate birthday) {
		super();
		Firstname = firstname;
		Lastname = lastname;
		Birthday = birthday;
		this.cars = new ArrayList<>();
	}

	public void addCars(car car) {
		this.cars.add(car);
	}

	public double getValue() {
		for (car car : cars) {
			price = price + car.carprice();

		}
		return price;
	}

	public int getAge() {
		Period p = Period.between(Birthday, today);
		return p.getYears();
	}
}
