package at.robin.car;

public class manufacturer {
	private String name;
	private String country;
	private float discount;

	public manufacturer(String name, String country, float discount) {
		super();
		this.name = name;
		this.country = country;
		this.discount = discount;
	}

	public manufacturer(float discount) {
		super();
		this.discount = discount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public float getDiscount() {
		return discount;
	}

	public void setDiscount(float discount) {
		this.discount = discount;
	}

}
