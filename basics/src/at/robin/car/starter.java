package at.robin.car;

import java.time.LocalDate;
import java.time.Month;
import java.util.Date;
import java.util.List;

public class starter {

	public static void main(String[] args) {

		manufacturer m1 = new manufacturer("noble", "britain", 50);

		car c1 = new car(230, "white", 7, "petrol", 100000, 2100, m1);
		car c2 = new car(230, "white", 7, "petrol", 50000, 2100, m1);

		Person p1 = new Person("Peter", "Lampert", LocalDate.of(1960, Month.JANUARY, 1));

		p1.addCars(c1);
		p1.addCars(c2);

		System.out.println(c1.carprice());
		System.out.println(c1.getMileage());

		System.out.println(p1.getValue());

		System.out.println(p1.getAge());
	}

}
