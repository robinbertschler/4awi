package basics;

public class starter {

	public static void main(String[] args) {
		rectangle r1 = new rectangle(4, 2);
		rectangle r2 = new rectangle(7, 3);
		rectangle r3 = r2;

		r1.sayHello();
		r2.sayHello();
		r3.sayHello();
		System.out.println("Meine flaeche betreagt: " +r1.getArea());
		System.out.println("Mein Umfang betreagt: " +r1.getCircumference());
	}

}
